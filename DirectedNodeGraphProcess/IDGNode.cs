using System.Collections.Generic;

namespace DirectedNodeGraphProcess
{
    public interface IDGNode
    {
        public string ID { get; init; }
        public bool IsProcessed { get; set; }
        public List<string> ConnectedNodeIDs { get; set; }
    }
}
