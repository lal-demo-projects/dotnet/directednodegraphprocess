﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;

namespace DirectedNodeGraphProcess
{
    internal class Program
    {
        static DataTable DT = new DataTable();
        static void Main(string[] args)
        {
            Test();
            Console.WriteLine("Hello, World!");
            Console.ReadLine();
        }

        static void Test()
        {
            DGNodeProcess<Node> nodeProcess = new();
            Node basicNode = new() { ID = "basic", IsProcessed = false, ConnectedNodeIDs = new List<string>(), Value = null };
            List<Node> nodes = new List<Node>()
            {
                basicNode,
                new Node() { ID = "da", IsProcessed = false, ConnectedNodeIDs = new List<string>() { "basic" }, Formula = "(basic * 12) / 100", Value = null },
                new Node() { ID = "hra", IsProcessed = false, ConnectedNodeIDs = new List<string>() { "basic", "da" }, Formula = "basic + da", Value = null },
                new Node() { ID = "check", IsProcessed = false, ConnectedNodeIDs = new List<string>() { "basic" }, Formula = "basic > 5000", Value = null }
            };
            nodeProcess.ValidateNodes(nodes);
            var basicAmts = new int[] { 2000, 5000, 6000, 7000 };
            foreach (int amt in basicAmts)
            {
                basicNode.Value = amt;
                var result = nodeProcess.ProcessNode(nodes, (node, connectedList) => node.Value = Calculate(node, connectedList));
                string outData;
                outData = $"Basic - {nodes.First(x => x.ID == "basic").Value}";
                outData += $", DA - {nodes.First(x => x.ID == "da").Value}";
                outData += $", HRA - {nodes.First(x => x.ID == "hra").Value}";
                outData += $", Check 5000 - {nodes.First(x => x.ID == "check").Value}";
                Console.WriteLine(outData);
            }
        }

        static double Calculate(Node node, List<Node> connectedList)
        {
            if (connectedList.Count == 0) return node.Value.Value;
            string formula = node.Formula;
            connectedList.ForEach(x => formula = formula.Replace(x.ID, x.Value.ToString()));
            return Convert.ToDouble(DT.Compute(formula, null));
        }
    }

    public class Node : IDGNode
    {
        public string ID { get; init; }
        public bool IsProcessed { get; set; }
        public List<string> ConnectedNodeIDs { get; set; }
        public string Formula { get; set; } = string.Empty;
        public double? Value { get; set; }
    }
}
