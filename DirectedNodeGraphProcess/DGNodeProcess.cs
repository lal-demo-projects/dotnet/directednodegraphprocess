﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DirectedNodeGraphProcess
{
    public class DGNodeProcess<T> where T : notnull, IDGNode
    {
        public void Reset(List<T> Nodes) => Nodes.ForEach(x => x.IsProcessed = false);

        public void ValidateNodes(List<T> Nodes)
        {
            if (Nodes.Count == 0)
                throw new InvalidOperationException("No nodes found");
            if (Nodes.Any(x => x.ConnectedNodeIDs.Count == 0) == false)
                throw new InvalidOperationException("No entry nodes found (Nodes don't have a parent connected node)");
            var lookup = Nodes.ToDictionary(x => x.ID);
            foreach (var node in Nodes)
            {
                if (string.IsNullOrWhiteSpace(node.ID))
                    throw new InvalidOperationException("Invalid ID (Blank, Empty or null)");
                if (Nodes.Count(x => x.ID == node.ID) > 1)
                    throw new InvalidOperationException($"ID already exist '{node.ID}'");
                if (node.ConnectedNodeIDs.Any(x => string.IsNullOrWhiteSpace(x) || x == node.ID || (lookup.ContainsKey(x) == false)))
                    throw new InvalidOperationException($"Invalid connected node ID of '{node.ID}'");
            }
        }

        public List<T> ProcessNode(List<T> Nodes, Action<T, List<T>> process)
        {
            Reset(Nodes);
            List<T> processedNodes = new();
            List<T> unprocessedNodes = Nodes.ToList();
            Stack<T> nodesToProcess = new();
            unprocessedNodes.ForEach(x => { if (x.ConnectedNodeIDs.Count == 0) nodesToProcess.Push(x); });
            while (nodesToProcess.Count > 0)
            {
                T currentNode = nodesToProcess.Pop();
                List<T> connectedNodes = processedNodes.Where(x => currentNode.ConnectedNodeIDs.Contains(x.ID)).ToList();
                process(currentNode, connectedNodes);
                processedNodes.Add(currentNode);
                unprocessedNodes.Remove(currentNode);
                currentNode.IsProcessed = true;
                if (nodesToProcess.Count == 0 && unprocessedNodes.Count > 0)
                {
                    foreach (T selectedNode in unprocessedNodes)
                    {
                        if (selectedNode.ConnectedNodeIDs.All(x => processedNodes.Any(y => y.ID == x)))
                        {
                            nodesToProcess.Push(selectedNode);
                        }
                    }
                    if (nodesToProcess.Count == 0)
                        throw new InvalidOperationException("Unable to continue the process. Connected node mismatch.");
                }
            }
            return Nodes;
        }
    }
}
